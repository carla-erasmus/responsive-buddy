function getWebsiteUrl() {

  var websiteUrlInput = document.getElementById('js-websiteUrlInput');

  if(!websiteUrlInput) {
    return;
  }

  return websiteUrlInput.value;
}

function changeResolution(device) {

  var websiteWrapper = document.getElementById('js-websiteWrapper');
  websiteWrapper.className = device;
}

function previewWebsite() {

  var websiteWrapper = document.getElementById('js-websiteWrapper');
  var websiteUrl = getWebsiteUrl();

  if (!websiteUrl) {
    return;
  }

  websiteWrapper.src = websiteUrl;
}